/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.castorflex.openharmony.smoothprogressbar.sample.slice;

import fr.castorflex.openharmony.circularprogressbar.CircularProgressBar;

import fr.castorflex.openharmony.smoothprogressbar.sample.ResourceTable;
import fr.castorflex.openharmony.smoothprogressbar.SmoothProgressBar;
import fr.castorflex.openharmony.smoothprogressbar.utils.ResUtil;
import fr.castorflex.openharmony.smoothprogressbar.sample.MakeCustomAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

public class MainAbilitySlice extends AbilitySlice {
    private static final String APP_BUNDLE_NAME = "fr.castorflex.openharmony.smoothprogressbar.sample";

    private ComponentContainer rootLayout;

    private Button makeUrOwn, btn_start, btn_stop;

    private SmoothProgressBar pocket, gPlus, googleNow, gradient;

    private CircularProgressBar circularprogressbar;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
            .parse(ResourceTable.Layout_ability_main, null, false);
        ShapeElement rootbackground = new ShapeElement();
        rootbackground.setShape(ShapeElement.RECTANGLE);
        rootbackground.setRgbColor(new RgbColor(41, 61, 61));
        rootLayout.setBackground(rootbackground);
        makeUrOwn = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_makeurown);
        btn_start = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_start);
        btn_stop = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_stop);
        circularprogressbar = (CircularProgressBar) rootLayout.findComponentById(ResourceTable.Id_circularprogressbar);
        circularprogressbar.setColors(new int[] {
            ResUtil.getColor(getContext(), ResourceTable.Color_holo_blue_dark),
            ResUtil.getColor(getContext(), ResourceTable.Color_holo_green_dark),
            ResUtil.getColor(getContext(), ResourceTable.Color_holo_purple_dark),
            ResUtil.getColor(getContext(), ResourceTable.Color_holo_red_dark),
            ResUtil.getColor(getContext(), ResourceTable.Color_holo_yellow_dark)
        });
        gPlus = (SmoothProgressBar) rootLayout.findComponentById(ResourceTable.Id_gplus);
        gPlus.setColors(new int[] {
            ResUtil.getColor(this, ResourceTable.Color_holo_blue_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_green_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_purple_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_red_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_yellow_dark)
        });
        googleNow = (SmoothProgressBar) rootLayout.findComponentById(ResourceTable.Id_googleNow);
        googleNow.setColors(new int[] {
            ResUtil.getColor(this, ResourceTable.Color_holo_blue_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_green_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_purple_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_red_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_yellow_dark)
        });
        gradient = (SmoothProgressBar) rootLayout.findComponentById(ResourceTable.Id_gradient);
        gradient.setColors(new int[] {
            ResUtil.getColor(this, ResourceTable.Color_holo_blue_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_green_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_purple_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_red_dark),
            ResUtil.getColor(this, ResourceTable.Color_holo_yellow_dark)
        });
        pocket = (SmoothProgressBar) rootLayout.findComponentById(ResourceTable.Id_pocket);
        pocket.setColors(new int[] {
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_1),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_1),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_1),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_1),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_2),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_2),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_2),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_2),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_3),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_3),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_3),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_3),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_4),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_4),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_4),
            ResUtil.getColor(getContext(), ResourceTable.Color_pocket_color_4)
        });
        ShapeElement background = new ShapeElement();
        background.setShape(ShapeElement.RECTANGLE);
        background.setRgbColor(new RgbColor(163, 194, 194));
        makeUrOwn.setBackground(background);
        makeUrOwn.setTextColor(Color.BLACK);
        btn_start.setBackground(background);
        btn_start.setTextColor(Color.BLACK);
        btn_stop.setBackground(background);
        btn_stop.setTextColor(Color.BLACK);
        initViewClick();
        super.setUIContent(rootLayout);
    }

    private void initViewClick() {
        makeUrOwn.setClickedListener(component -> {
            Operation operation = new Intent.OperationBuilder().withBundleName(APP_BUNDLE_NAME)
                .withAbilityName(MakeCustomAbility.class.getSimpleName())
                .build();
            Intent intent = new Intent();
            intent.setOperation(operation);
            startAbility(intent);
        });
        btn_start.setClickedListener(component -> {
            pocket.progressiveStart();
        });
        btn_stop.setClickedListener(component -> {
            pocket.progressiveStop();
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
