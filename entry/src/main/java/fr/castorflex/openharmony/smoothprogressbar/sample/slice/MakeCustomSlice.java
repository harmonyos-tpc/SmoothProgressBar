/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.castorflex.openharmony.smoothprogressbar.sample.slice;


import com.afollestad.materialdialogs.MaterialDialog;

import fr.castorflex.openharmony.circularprogressbar.CircularProgressBar;
import fr.castorflex.openharmony.smoothprogressbar.SmoothProgressBar;
import fr.castorflex.openharmony.smoothprogressbar.curvetype.CurveTypes;
import fr.castorflex.openharmony.smoothprogressbar.curvetype.TimeCurveType;
import fr.castorflex.openharmony.smoothprogressbar.curvetype.LinearCurveType;
import fr.castorflex.openharmony.smoothprogressbar.curvetype.FastOutSlowCurveType;
import fr.castorflex.openharmony.smoothprogressbar.curvetype.AccelerateDecelerateCurveType;
import fr.castorflex.openharmony.smoothprogressbar.curvetype.AccelerateCurveType;
import fr.castorflex.openharmony.smoothprogressbar.curvetype.DecelerateCurveType;
import fr.castorflex.openharmony.smoothprogressbar.sample.ResourceTable;
import fr.castorflex.openharmony.smoothprogressbar.utils.ResUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Text;
import ohos.agp.components.Button;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Slider;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.global.configuration.Configuration;


import java.util.Locale;

public class MakeCustomSlice extends AbilitySlice {
    private ComponentContainer rootLayout;
    private Button start, stop;
    private Slider mSpeed, mStrokeWidth, mSectionsCount, mSeperatorLength, mFactor;
    private Checkbox mReversed, mMirrorMode, mGradient;
    private SmoothProgressBar mSmoothProgressBar;
    private CircularProgressBar mCircularProgressBar;
    private int mStrokeDefaultWidth = 4;
    private float mSpeedDefault = 1f;
    private int mSectionsDefaultCount;
    private int mSeperatorDefaultLength;
    private float mFactorDefault = 1f;
    private Text mStrokeTextWidth, mSpeedText, mSectionsCountText, mSeperatorText, mFactorText, dropdownText;
    private Image dropdown;
    private TimeCurveType mCurrentCurveType;
    private int mCurveType;
    private Color mSeekBarColor;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_custom, null, false);

        mSmoothProgressBar = (SmoothProgressBar) rootLayout.findComponentById(ResourceTable.Id_smoothprogressbar);
        mSmoothProgressBar.setColors(new int[]{ResUtil.getColor(this, ResourceTable.Color_holo_blue_dark),
                ResUtil.getColor(this, ResourceTable.Color_holo_green_dark),
                ResUtil.getColor(this, ResourceTable.Color_holo_purple_dark),
                ResUtil.getColor(this, ResourceTable.Color_holo_red_dark),
                ResUtil.getColor(this, ResourceTable.Color_holo_yellow_dark)});
        mCircularProgressBar = (CircularProgressBar) rootLayout.findComponentById(ResourceTable.Id_circularprogressbar);
        mCircularProgressBar.setColors(new int[]{ResUtil.getColor(getContext(), ResourceTable.Color_holo_blue_dark),
                ResUtil.getColor(getContext(), ResourceTable.Color_holo_green_dark),
                ResUtil.getColor(getContext(), ResourceTable.Color_holo_purple_dark),
                ResUtil.getColor(getContext(), ResourceTable.Color_holo_red_dark),
                ResUtil.getColor(getContext(), ResourceTable.Color_holo_yellow_dark)});
        int orientation = this.getResourceManager().getConfiguration().direction;
        if (orientation == Configuration.DIRECTION_VERTICAL) {
            mSmoothProgressBar.setWidth(1000);
            mSmoothProgressBar.setHeight(48);
        } else {
            mSmoothProgressBar.setWidth(2000);
            mSmoothProgressBar.setHeight(48);
        }

        ShapeElement rootbackground = new ShapeElement();
        rootbackground.setShape(ShapeElement.RECTANGLE);
        rootbackground.setRgbColor(new RgbColor(41, 61, 61));
        rootLayout.setBackground(rootbackground);
        start = (Button) rootLayout.findComponentById(ResourceTable.Id_start);
        stop = (Button) rootLayout.findComponentById(ResourceTable.Id_stop);
        dropdownText = (Text) rootLayout.findComponentById(ResourceTable.Id_dropdownText);

        ShapeElement background = new ShapeElement();
        background.setShape(ShapeElement.RECTANGLE);
        background.setRgbColor(new RgbColor(163, 194, 194));
        start.setBackground(background);
        start.setTextColor(Color.BLACK);
        stop.setBackground(background);
        stop.setTextColor(Color.BLACK);

        mReversed = (Checkbox) rootLayout.findComponentById(ResourceTable.Id_reversed);
        mMirrorMode = (Checkbox) rootLayout.findComponentById(ResourceTable.Id_mirror);
        mGradient = (Checkbox) rootLayout.findComponentById(ResourceTable.Id_gradients);

        mStrokeTextWidth = (Text) rootLayout.findComponentById(ResourceTable.Id_stroketextwidth);
        mSpeedText = (Text) rootLayout.findComponentById(ResourceTable.Id_speedtext);
        mSeperatorText = (Text) rootLayout.findComponentById(ResourceTable.Id_seperatortext);
        mSectionsCountText = (Text) rootLayout.findComponentById(ResourceTable.Id_sectionscounttext);
        mFactorText = (Text) rootLayout.findComponentById(ResourceTable.Id_factortext);

        mSpeed = (Slider) rootLayout.findComponentById(ResourceTable.Id_speed);
        mStrokeWidth = (Slider) rootLayout.findComponentById(ResourceTable.Id_strokewidth);
        mSectionsCount = (Slider) rootLayout.findComponentById(ResourceTable.Id_sectionscount);
        mSeperatorLength = (Slider) rootLayout.findComponentById(ResourceTable.Id_seperatorlength);
        mFactor = (Slider) rootLayout.findComponentById(ResourceTable.Id_factor);

        dropdown = (Image) rootLayout.findComponentById(ResourceTable.Id_dropdown);
        ShapeElement imageBackground = new ShapeElement();
        imageBackground.setRgbColor(new RgbColor(163, 194, 194));
        dropdown.setBackground(imageBackground);
        mSeekBarColor = mFactor.getProgressColor();


        start.setClickedListener(component -> {
            mSmoothProgressBar.progressiveStart();
            mCircularProgressBar.progressiveStart();
        });

        stop.setClickedListener(component -> {
            mSmoothProgressBar.progressiveStop();
            mCircularProgressBar.progressiveStop();
        });

        mReversed.setCheckedStateChangedListener((absButton, b) -> {
            mSmoothProgressBar.setSmoothProgressDrawableReversed(b);
        });

        mMirrorMode.setCheckedStateChangedListener((absButton, b) -> {
            mSmoothProgressBar.setSmoothProgressDrawableMirrorMode(b);
        });

        mGradient.setCheckedStateChangedListener((absButton, b) -> {
            mSmoothProgressBar.setSmoothProgressDrawableUseGradients(b);
        });

        mSpeed.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                mSpeedDefault = ((float) progress + 1) / 10;
                mSpeedText.setText("Speed: " + mSpeedDefault);
                mSmoothProgressBar.setSmoothProgressDrawableSpeed(mSpeedDefault);
                mSmoothProgressBar.setSmoothProgressDrawableProgressiveStartSpeed(mSpeedDefault);
                mSmoothProgressBar.setSmoothProgressDrawableProgressiveStopSpeed(mSpeedDefault);
                mCircularProgressBar.setCircularProgressDrawableSpeed(mSpeedDefault);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        mStrokeWidth.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                mStrokeDefaultWidth = progress;
                mStrokeTextWidth.setText(String.format(Locale.US, "Stroke width: %ddp", mStrokeDefaultWidth));
                mSmoothProgressBar.setSmoothProgressDrawableStrokeWidth(mStrokeDefaultWidth);
                mCircularProgressBar.setCircularProgressDrawableStrokeWidth(mStrokeDefaultWidth);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        mSectionsCount.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                mSectionsDefaultCount = progress + 1;
                mSectionsCountText.setText("Sections count: " + mSectionsDefaultCount);
                mSmoothProgressBar.setSmoothProgressDrawableSectionsCount(mSectionsDefaultCount);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        mSeperatorLength.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                mSeperatorDefaultLength = progress;
                mSeperatorText.setText(String.format(Locale.US, "Separator length: %ddp", mSeperatorDefaultLength));
                mSmoothProgressBar.setSmoothProgressDrawableSeparatorLength(mSeperatorDefaultLength);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        dropdown.setClickedListener(component -> {
                    new MaterialDialog.Builder(this)
                    .title(ResourceTable.String_select_curve_type)
                    .items(new String[]{CurveTypes.valueOf(0).toString(), CurveTypes.valueOf(1).toString(),
                            CurveTypes.valueOf(2).toString(), CurveTypes.valueOf(3).toString(),
                            CurveTypes.valueOf(4).toString()})
                    .itemsCallbackSingleChoice(0, (dialog, view, which, text) -> {
                        dropdownText.setText(text.toString());
                        getPosition(text.toString());
                        return true;
                    })
                    .positiveText(ResourceTable.String_md_choose_label)
                    .show();
        });

        mFactor.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                mFactorDefault = (progress + 1) / 10f;
                mFactorText.setText("Factor: " + mFactorDefault);
                setCurveType(mCurveType, mFactorDefault);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
        super.setUIContent(rootLayout);
    }

    private void getPosition(String text) {
        switch (text) {
            case "Linear": {
                setCurveType(3, mFactorDefault);
                mCurveType = 0;
                break;
            }
            case "Accelerate": {
                setCurveType(1, mFactorDefault);
                mCurveType = 1;
                break;
            }
            case "Decelerate": {
                setCurveType(2, mFactorDefault);
                mCurveType = 2;
                break;
            }
            case "AccelerateDecelerate": {
                setCurveType(3, mFactorDefault);
                mCurveType = 3;
                break;
            }
            case "FastOutSlowIn": {
                setCurveType(4, mFactorDefault);
                mCurveType = 4;
                break;
            }
            default: {
                setCurveType(0, mFactorDefault);
                mCurveType = 0;
            }
        }
    }

    private void setCurveType(int position, float factor) {
        switch (position) {
            case 0: {
                mCurrentCurveType = new LinearCurveType();
                mFactor.setEnabled(false);
                Color color = mFactor.getProgressColor();
                mFactor.setProgressColor(Color.GRAY);
                break;
            }
            case 1: {
                mCurrentCurveType = new AccelerateCurveType(factor);
                mFactor.setEnabled(true);
                mFactor.setProgressColor(mSeekBarColor);
                break;
            }
            case 2: {
                mCurrentCurveType = new DecelerateCurveType(factor);
                mFactor.setEnabled(true);
                mFactor.setProgressColor(mSeekBarColor);
                break;
            }
            case 3: {
                mCurrentCurveType = new AccelerateDecelerateCurveType();
                mFactor.setEnabled(false);
                mFactor.setProgressColor(Color.GRAY);
                break;
            }
            case 4: {
                mCurrentCurveType = new FastOutSlowCurveType(factor);
                mFactor.setEnabled(false);
                mFactor.setProgressColor(Color.GRAY);
                break;
            }
            default:
                mCurrentCurveType = new LinearCurveType();
                mFactor.setEnabled(false);
                mFactor.setProgressColor(Color.GRAY);
                break;
        }
        mSmoothProgressBar.setSmoothProgressDrawablecurvetype(mCurrentCurveType, factor);
    }
}
