package fr.castorflex.openharmony.smoothprogressbar;

import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;

/**
 * Created by castorflex on 3/5/14.
 */
public class ColorsShape extends ShapeElement {

    private float mStrokeWidth;
    private int[] mColors;
    private Paint mPaint;

    public ColorsShape(float strokeWidth, int[] colors) {
        mStrokeWidth = strokeWidth;
        mColors = colors;
        mPaint = new Paint();
    }

    public float getStrokeWidthShape() {
        return mStrokeWidth;
    }

    public void setStrokeWidth(float strokeWidth) {
        mStrokeWidth = strokeWidth;
    }

    public int[] getColors() {
        return mColors;
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
        float ratio = 1f / mColors.length;
        int i = 0;
        mPaint.setStrokeWidth(mStrokeWidth);
        for (int color : mColors) {
            mPaint.setColor(new Color(color));
            canvas.drawLine(new Point(i * ratio * getWidth(), getHeight() / 2),
                    new Point(++i * ratio * getWidth(), getHeight() / 2), mPaint);

        }
    }

    public void setColors(int[] colors) {
        mColors = colors;
    }
}
