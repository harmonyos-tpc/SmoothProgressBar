/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.castorflex.openharmony.smoothprogressbar.curvetype;

import java.util.HashMap;
import java.util.Map;

public enum CurveTypes {
    Linear(0),
    Accelerate(1),
    Decelerate(2),
    AccelerateDecelerate(3),
    FastOutSlowIn(4);

    private int value;
    private static Map map = new HashMap<>();

    private CurveTypes(int value) {
        this.value = value;
    }

    static {
        for (CurveTypes curveTypes : CurveTypes.values()) {
            map.put(curveTypes.value, curveTypes);
        }
    }

    public static CurveTypes valueOf(int curveType) {
        return (CurveTypes) map.get(curveType);
    }

    public int getValue() {
        return value;
    }
}
