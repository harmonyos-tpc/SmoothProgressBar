# SmoothProgressBar

## Introduction:
Small library allowing you to make a smooth indeterminate progress bar.
You can either use your progress bars and set this drawable or use directly the SmoothProgressBarView

## Usage Instructions:
1. Add library.har,library_circular.har and core.har file in the entry libs folder & add the same in build.gradle file:

        implementation files('libs/library.har')
        implementation files('libs/library_circular.har')
        implementation 'io.openharmony.tpc.thirdlib:material-dialogs:1.0.2'


##### XML

Use directly SmoothProgressBar:

		  <fr.castorflex.harmony.smoothprogressbar.SmoothProgressBar
                  ohos:height="20vp"
                  ohos:width="match_parent"
                  ohos:spb_default_mirror_mode="true"
                  ohos:spb_default_progressiveStart_activated="true"
                  ohos:spb_default_reversed="true"
                  ohos:spb_default_sections_count="2"
                  ohos:spb_default_speed="1.7"
                  ohos:spb_default_stroke_separator_length="8vp"
                  ohos:spb_default_stroke_width="12.0"
                  ohos:spb_generate_background_with_colors="false"
                  ohos:spb_curvetype="8"
                  ohos:spb_progressiveStart_speed="2"
                  ohos:spb_progressiveStop_speed="3.4"
                  ohos:top_margin="10vp"/>

          <fr.castorflex.harmony.circularprogressbar.CircularProgressBar
                   ohos:height="100vp"
                   ohos:width="100vp"
                   ohos:center_in_parent="true"
                   ohos:layout_alignment="center"/>

### Customization
** Setting user defined colors

           SmoothProgressBar pocket = (SmoothProgressBar)rootLayout.
           findComponentById(ResourceTable.Id_pocket);
           pocket.setColors(new int[] {colors});

** Currently supported LinearCurveType, AccelerateCurveType, DecelerateCurveType, AccelerateDecelerateCurveType
   Need to pass CurveType values as follows

                LinearCurveType - 0
        		AccelerateCurveType - 1
        		DecelerateCurveType - 2
        		AccelerateDecelerateCurveType - 3
        		FastOutSlowIn - 4

## Installation instruction
Method 1:
Generate the .har package through the library and add the .har package to the libs folder.
Add the following code to the entry gradle:
```
implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])
```
Method 2:
For using as a library project, add the following code to the entry gradle:
```
implementation project(path: ': library)
implementation project(path: ': library_circular)
implementation 'io.openharmony.tpc.thirdlib:material-dialogs:1.0.2'
```
Method 3. For using SmoothProgressBar library from a remote repository in separate application, add the below dependency in entry/build.gradle file.
``` 
SmoothProgressBar library:
      
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:SmoothProgressBar-Library:1.0.0'
}

library_circular:

dependencies {
    implementation 'io.openharmony.tpc.thirdlib:SmoothProgressBar:1.0.0'
}
```

## License

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.