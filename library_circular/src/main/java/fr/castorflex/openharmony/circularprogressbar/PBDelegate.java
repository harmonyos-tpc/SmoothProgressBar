package fr.castorflex.openharmony.circularprogressbar;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;

interface PBDelegate {

    void draw(Canvas canvas, Paint paint);

    void start();

    void stop();

    void progressiveStop(CircularProgressDrawable.OnEndListener listener);

    void setSpeed(float speed);

    void setRotation(float speed);
}
