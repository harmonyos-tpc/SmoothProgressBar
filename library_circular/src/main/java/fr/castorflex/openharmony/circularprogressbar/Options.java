package fr.castorflex.openharmony.circularprogressbar;

class Options {
    //params
    final int mCurveType;
    final float borderWidth;
    final int[] colors;
    final float sweepSpeed;
    final float rotationSpeed;
    final int minSweepAngle;
    final int maxSweepAngle;
    final int style;

    Options(int angleCurveType,
            float borderWidth,
            int[] colors,
            float sweepSpeed,
            float rotationSpeed,
            int minSweepAngle,
            int maxSweepAngle,
            int style) {
        this.mCurveType = angleCurveType;
        this.borderWidth = borderWidth;
        this.colors = colors;
        this.sweepSpeed = sweepSpeed;
        this.rotationSpeed = rotationSpeed;
        this.minSweepAngle = minSweepAngle;
        this.maxSweepAngle = maxSweepAngle;
        this.style = style;
    }
}
