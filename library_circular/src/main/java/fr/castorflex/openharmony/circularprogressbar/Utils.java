package fr.castorflex.openharmony.circularprogressbar;

import ohos.app.Context;
import ohos.powermanager.PowerManager;
import org.jetbrains.annotations.NotNull;

import java.util.Locale;

import static java.lang.Math.min;

class Utils {

    private Utils() {
    }

    static void checkSpeed(float speed) {
        if (speed <= 0f)
            throw new IllegalArgumentException("Speed must be >= 0");
    }

    static void checkColors(int[] colors) {
        if (colors == null || colors.length == 0)
            throw new IllegalArgumentException("You must provide at least 1 color");
    }

    static void checkAngle(int angle) {
        if (angle < 0 || angle > 360)
            throw new IllegalArgumentException(String.format(Locale.US, "Illegal angle %d: must be >=0 and <=360", angle));
    }

    static void checkPositiveOrZero(float number, String name) {
        if (number < 0)
            throw new IllegalArgumentException(String.format(Locale.US, "%s %f must be positive", name, number));
    }

    static void checkPositive(int number, String name) {
        if (number <= 0)
            throw new IllegalArgumentException(String.format(Locale.US, "%s must not be null", name));
    }

    static void checkNotNull(Object o, String name) {
        if (o == null)
            throw new IllegalArgumentException(String.format(Locale.US, "%s must be not null", name));
    }

    static boolean isPowerSaveModeEnabled(@NotNull PowerManager powerManager) {
        try {
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    static PowerManager powerManager(Context context) {
        return null;
    }
}
