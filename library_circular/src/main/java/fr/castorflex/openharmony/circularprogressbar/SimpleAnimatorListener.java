package fr.castorflex.openharmony.circularprogressbar;

import ohos.agp.animation.Animator;

abstract class SimpleAnimatorListener implements Animator.StateChangedListener {
    private boolean mStarted = false;
    private boolean mCancelled = false;

    @Override
    public void onStart(Animator animator) {
        mCancelled = false;
        mStarted = true;
    }

    @Override
    public void onStop(Animator animator) {
    }

    @Override
    public void onCancel(Animator animator) {
        mCancelled = true;
    }

    @Override
    public void onEnd(Animator animator) {
        onPreAnimationEnd(animator);
        mStarted = false;
    }

    protected void onPreAnimationEnd(Animator animator) {
    }

    @Override
    public void onPause(Animator animator) {

    }

    @Override
    public void onResume(Animator animator) {

    }

    public boolean isStartedAndNotCancelled() {
        return mStarted && !mCancelled;
    }
}
