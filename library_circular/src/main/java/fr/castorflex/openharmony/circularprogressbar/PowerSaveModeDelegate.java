package fr.castorflex.openharmony.circularprogressbar;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

/**
 * Created by castorflex on 9/12/15.
 */
class PowerSaveModeDelegate implements PBDelegate {
    private static final long REFRESH_RATE = TimeUnit.SECONDS.toMillis(1L);

    private final CircularProgressDrawable mParent;
    private int mCurrentRotation;

    PowerSaveModeDelegate(@NotNull CircularProgressDrawable parent) {
        mParent = parent;
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
    }

    @Override
    public void start() {
        mParent.invalidateSelf();
    }

    @Override
    public void stop() {
    }

    @Override
    public void progressiveStop(CircularProgressDrawable.OnEndListener listener) {
        mParent.stop();
    }

    @Override
    public void setSpeed(float speed) {

    }

    @Override
    public void setRotation(float speed) {

    }

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mCurrentRotation += 50;
            mCurrentRotation %= 360;
            mParent.invalidateSelf();
        }
    };
}
